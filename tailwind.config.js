// This is for the production
// We can now set our own config options. Currently, the config file below uses purge to remove every unused CSS in production:
const production = !process.env.ROLLUP_WATCH;
module.exports = {
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  plugins: [],
  purge: {
    content: ['./src/*'],
    enabled: production, // disable purge in dev
  },
};
