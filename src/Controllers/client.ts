import path from 'path'
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';

import { ProtoGrpcType } from '../Proto/ChatProto';

const PROTO_FILE_DIR = '../Proto/ChatProto.proto'

const PORT = 8083;

// Instantiating chatPackage
const packageDef = protoLoader.loadSync(path.resolve(__dirname, PROTO_FILE_DIR))
const grpcObj = (grpc.loadPackageDefinition(packageDef) as unknown) as ProtoGrpcType



const client = new grpcObj.chatProto.ChatApp(`0.0.0.0:${PORT}`, grpc.credentials.createInsecure())



const deadline = new Date();
deadline.setSeconds(deadline.getSeconds() + 5)
client.waitForReady(deadline, (err) => {
    if (err) {
        console.error(err)
        return
    }
    onClientReady();
})

const onClientReady = () => {
    client.allChats({ message: "Im from the client " }, (err, result) => {
        if (err) {
            console.error(err)
            return
        }
        console.log(result)
    })
}